# Pythowo  -  The cuwute pwogwamming language
## Table of contents
* [Installation](https://codeberg.org/LinuxPropaganda/pythowo#installation)
* [Usage](https://codeberg.org/LinuxPropaganda/pythowo#usage)
* [How to code](https://codeberg.org/LinuxPropaganda/pythowo#how-to-code-in-pythowo)
	* [Syntax](https://codeberg.org/LinuxPropaganda/pythowo#syntax)
* [Running scripts](https://codeberg.org/LinuxPropaganda/pythowo#interpreter)
* [Contributing](https://codeberg.org/LinuxPropaganda/pythowo#contributing)
* [Inspiration](https://codeberg.org/LinuxPropaganda/pythowo#inspiration)

#### Logo
![PythowoLogo](https://preview.redd.it/dclacd1t6t791.png?width=960&crop=smart&auto=webp&s=dc09b59693ad091b7ece9fd9932f198e47895852)

## Installation

* Requires python 3.8 or highier
* Run ```sudo ./install.sh``` that will create the proper file in /usr/bin
* You're all done!


## Usage

* To run a script, type `pythowo <file.owo> [flags]`
* Run `pythowo -h` for further information


## How to code in Pythowo

This language is fully python compatible. This means that you can run python scripts with pythowo, as long as you disable Profanity Error (so you can use the letter 'r') with `--swear`.
However, it hase some features of its own.

### Comments
Any line containing the sequence `UwU` will be turned into a comment
Example:
```
	This line woun't be evalUwUated
```

### Syntax
It's generally the python one, except you can use

* `(｡･ω･｡)ﾉ♡` instead of `;`
* `uwu` instead of `u`
* `owo` instead of `o`
* `w` instead of `r`
* `^w` to use a `w` and not an `r` in the output</br>
(the last two work for caps as well `W->R` and `^W->W`)

and you cannot use `r` or `R`.

Also some statements got uwuified:

variable declaration/assignation: `please set <name> to <value>`
that can be chained like so: `please set <name1> to <name2> to <value>` assigning `<value>` to both variable

functions:
here's some fUwUncitons
```
task myFunction(arg1, arg2:type, *args, keyword1=defaultvalue1, **kwargs): -> type
	
	...

	wetuwn <return code>
```
(task is a deltarune chapter 2 reference btw)

and classes:
```
kitty MyClass:
	task __init__(self, ...):
		...

	...
```
### Interpreter
once you have installed pythowo, you can add `#! /bin/pythowo` at the start of the file, then run `chmod +x <filename>` to make it executable as
`./<filename>`, using automatically pythowo as "interpreter".
With that said, is preferrable during development to run your script with `pythowo <filename>`, so you can use arguments while you code.

## Contributing
There's plenty more to uwuify, feel free to share suggestions or implement them yourself and pull request!

### Where to look in main.py

The first few lines are initialization and we can safely ignore them.
* Lines 27 to 89 are the configuration and the core of the language. You will most likely want to change stuff there.
* You may also wanna have a look at lines 111 to 134

### Understanding the transpiling process

The program will modify the .owo file into a python script. It will achieve this by firstly doing a bunch of find+replace,
as indicated by replace_deez_nuts dictionary, IN ORDER, example
```
impowowt wandowom
```
would first replace `owo` into `o`, then the remaining `w`s into `r`s, generating the following python code
```
import random
```
then by applying some more changes that are hard coded and not in the config part.



## Inspiration

[Reddit Post]( https://www.reddit.com/r/ProgrammerHumor/comments/vkkyyv/say_hello_to_pythowo_make_sure_to_treat_her_well )