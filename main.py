#! /bin/python3

# Copyright (c) 2022 Lorenzo Mari
# GNU GPL version 3-or-later
# This is free software and comes with absolutely NO WARRANTY of any kind

# pythowo transpiler + debugger


from sys import argv, exit
import os, traceback

def main():

	ParseArgs()
	Output(UwU_to_PY())


def reverse_dict(original_dict):
	# reverse order + invert way
	key_list = [i for i in reversed(list(original_dict.keys()))]
	val_list = [i for i in reversed(list(original_dict.values()))]
	
	return dict([(name, key_list[i]) for i, name in enumerate(val_list)])


## CONFIG

additional_error_info = {
	"Arguments" : "Use -h to get more info",
	"Profanity" : "Little kitty cannot pronounce that >:(. (use --swear to enable r, but be aware you're an horrible person)",

	"IndexError": "Remember to start counting from 0!",
	"NameError" : "Check spelling? Missing quotes?",
	"TypeError" : "Seems like you're working with different types"
}


HELP = '''
usage:
	command <source.owo> [flags]

	flags:
		--mode <r/s>	: 'run'/'save as python3', can be both (-m rs).  default r. 
		--out <target>	: file to save translated code to, must be used with save mode. default out.py
		--hide-tips		: do not show tips on errors
		--swear			: allow the use of 'r' and 'R' chars 
		-h				: show this message and exit

'''

# accessed by arguments
flags = {
	"mode" : "r",
	"out"  : "out.py",
	"hide-tips" : False,
	"swear": False,
}
friendly_name = {
	"mode" : "mode (r/s/rs)",
	"out"  : "output file",
}



# order MATTERS
replace_deez_nuts = {

	"UwU": "COMMENTTHISLINEPLS",

	"uwu": 'u',
	"owo": 'o',
	
	"task ": 'def ',
	"kitty": 'class',

	"(｡･ω･｡)ﾉ♡": ';', # as requested by u/DrainZ-


	"^W": "~ESCAPED~",
	"W" : "R",
	"~ESCAPED~": "W",

	"^w": "~escaped~",
	"w" : "r",
	"~escaped~": "w",


}
nuts_deez_replace = reverse_dict(replace_deez_nuts)

## END CONFIG


def UwU_to_PY():

	out = source

	for src, dest in replace_deez_nuts.items():
		out = out.replace(src, dest)

	out = out.replace(';\n', "\n")

	lines = out.split('\n')
	for i, wholeline in enumerate(lines):
		#print(repr(line)) # debug


		indent = ""

		rebuilt_line = []
		for line in wholeline.split(';'):
			while len(line) and line[0] in [' ', '\t']:
				indent += line[0]
				line = line[1:]


			# re enable python2 print statement because y not
			if "print " in line:
				lines[i] = line.replace("print ", "print(") + ')'

			# and now let's have a little fun!

			# comments (the UwU can be anywhere in the comment)
			if "COMMENTTHISLINEPLS" in line:
				#print(line)
				lines[i] = '# ' + line.replace("COMMENTTHISLINEPLS", '')

			if line[0:11] == "please set ":
				lines[i] = line[11:].replace('to', '=')

			rebuilt_line.append(lines[i].strip(' ').strip('\t'))

		lines[i] = indent + ';'.join(rebuilt_line)


	return '\n'.join(lines)




def ParseArgs():

	global source, in_file

	args = argv[1:]

	if '-h' in args:
		print(HELP); exit(0)


	to_remove = []
	for i, arg in enumerate(args):
		if arg[:2] == '--':

			arg = arg[2:]
			#print(arg)

			if arg in flags:
				if type(flags[arg]) == bool:
					flags[arg] = 1
					to_remove.append(args[i])
				else:
					if len(args)<=i+1 or '-' in args[i+1]:
						error(friendly_name.get(arg, arg), "not specified", name="Arguments")

					# next 3 lines aren't really necessary, but if i ever need an int arg they'll come in handy
					try:
						flags[arg] = type(flags[arg])(args[i+1])
					except: error(friendly_name.get(arg, arg), "invalid type, expected", type(flags[arg]).__name__.replace('r', 'W'), name="Arguments")
					#

					to_remove.append(args[i])
					if len(args)>i+1:
						to_remove.append(args[i+1])

	#print(to_remove, "\n", flags)
	for i in to_remove:
		if i in args: args.remove(i)

	if not len(args):
		error("no input file pwovided", name="Arguments")

	in_file = args[0]
	if not os.path.exists(in_file):
		error(in_file, name="File not found")

	with open(in_file, 'r') as f:
		source = f.read()

	if not flags["swear"] and "r" in source.lower():
		error("YOU USED AN r! HOW DAWE YOU!!", name="Profanity")



def Output(out):
	if 's' in flags["mode"]:
		with open(flags["out"], 'w') as f:
			f.write(out)

	if 'r' in flags["mode"]:
		try:
			exec(out)
		except KeyboardInterrupt:
			print()
			error("You killed da kitty :'(", name="KeyboardInterrupt")

		except Exception as e:
			raw_error = traceback.format_exc()

			display_error = raw_error.split('File "<string>"')[1]
			error(f"OH NO!\nIn {in_file} {display_error}", name=type(e).__name__)



def error(*args, name=''):

	gimme_error = "error" not in name.lower()
	gimme_space = bool(name) and gimme_error

	ignore = ["~escaped~", "~ESCAPED~", "W", "w", "o", "u"]
	if flags["swear"]:
		ignore = ["R", "r"]


	uwuname = name
	args = [str(i) for i in args]
	for src, dest in nuts_deez_replace.items():
		if src in ignore:
			continue
		uwuname = uwuname.replace(src, dest)
		args = [i.replace(src, dest) for i in args]

	print("\33[1;31m[ "+uwuname+' '*gimme_space+"Ewwow"*gimme_error+" ] \33[0m", *args)
	if not flags["hide-tips"]: print("uwu~", additional_error_info.get(name, 'no info on this ewwow'))	
	exit(1)






if __name__ == '__main__':
	main()